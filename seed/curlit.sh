#!/bin/bash

if [ -z "$1" ]
then
	server="http://localhost:5984"
else
	server="$1"
fi

for json in `ls *.json`
do
	db="`echo $json | sed s/.json$//`"
	echo curl -X DELETE $server/$db
	curl -X DELETE $server/$db
	echo curl -X PUT $server/$db
	curl -X PUT $server/$db
	echo curl -X POST                       \
	     -d @$json                          \
	     -H Content-type:\ application/json \
		 $server/$db/_bulk_docs

	curl -X POST                            \
	     -d @$json                          \
	     -H Content-type:\ application/json \
		 $server/$db/_bulk_docs
done

