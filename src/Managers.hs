module Managers ( Managers(..) 
                , createManagers ) where

import Network.HTTP.Client

data Managers = Managers { mgrDB :: Manager
                         , mgrAPI :: Manager
                         }

createManagers :: IO Managers
createManagers = do
    mgrDB <- newManager defaultManagerSettings
    mgrAPI <- newManager defaultManagerSettings
    return $ Managers mgrDB mgrAPI
