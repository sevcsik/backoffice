module Session
    ( createSession 
    , SessionError (..)
    ) where

import CouchDB
import Model.User (Namespace)
import Model.Session

import Data.Time.Clock
import Data.Aeson
import Data.Text

createSession :: Env -> Namespace -> DocId -> IO (Either SessionError Session)
createSession env ns userId = do
    start <- getCurrentTime
    let end = addUTCTime (60 * 60 * 24) start
    let session = Session { sessionNamespace = ns
                          , sessionStart     = start
                          , sessionEnd       = end
                          , sessionUserId    = userId
                          , sessionMetadata  = Nothing
                          }
    session' <- save env session
    case session' of
        Left error -> return $ Left $ SessionDBError error
        Right session -> return $ Right session

data SessionError = SessionDBError Error

instance ToJSON SessionError where
    toJSON (SessionDBError error) = 
        object $ [ "type"  .= ("session_db_error" :: Text)
                 , "error" .= error
                 ]                            
