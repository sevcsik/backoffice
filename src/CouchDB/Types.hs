module CouchDB.Types 
    ( module CouchDB.Types
    , module Data.Default.Class
    ) where

import EnvSettings
import Data.Default.Class
import Data.Aeson.Types
import Control.Monad (mzero)
import qualified Data.Text as T
import Network.HTTP.Client ( Manager, HttpException)
import qualified Network.HTTP.Types as HTTP
import qualified Data.ByteString as BS

type DocId = T.Text
type Path = T.Text

data Rev = Rev T.Text
instance FromJSON Rev where
    parseJSON (Object v) = Rev <$> v.: "rev"
    parseJSON _ = mzero

data Settings = Settings { settingsURL          :: T.Text
                         , settingsUsername     :: Maybe T.Text
                         , settingsPassword     :: Maybe T.Text
                         , settingsLogRequests  :: Bool
                         , settingsLogResponses :: Bool
                         }

instance Default Settings where
    def = Settings 
        { settingsURL = "http://localhost:5984/sampledb"
        , settingsUsername = Nothing
        , settingsPassword = Nothing
        , settingsLogRequests = True
        , settingsLogResponses = True
        }

instance EnvSettings Settings where
    set "URL" val st = st { settingsURL = T.pack val }
    set "USERNAME" val st = st { settingsUsername = Just $ T.pack val }
    set "PASSWORD" val st = st { settingsPassword = Just $ T.pack val }
    set "LOG_REQUESTS" val st = st { settingsLogRequests = val == "true" }
    set "LOG_RESPONSES" val st = st { settingsLogResponses = val == "true" }
    parse prefix = updateWithList prefix [ "URL"
                                         , "USERNAME"
                                         , "PASSWORD"
                                         , "LOG_REQUESTS"
                                         , "LOG_RESPONSES"
                                         ]

data Env = Env { envSettings :: Settings
               , envManager :: Manager
               }

data Query key = Query { queryKey           :: Maybe key
                       , queryKeys          :: Maybe [key]
                       , queryStartkey      :: Maybe key
                       , queryStartkeyDocid :: Maybe DocId
                       , queryEndkey        :: Maybe key
                       , queryEndkeyDocid   :: Maybe DocId
                       , queryLimit         :: Maybe Int
                       , queryStale         :: Maybe QueryStale
                       , queryDescending    :: Maybe Bool
                       , querySkip          :: Maybe Int
                       , queryGroup         :: Maybe Bool
                       , queryGroupLevel    :: Maybe Int
                       , queryReduce        :: Maybe Bool
                       , queryIncludeDocs   :: Maybe Bool
                       , queryInclusiveEnd  :: Maybe Bool
                       , queryUpdateSeq     :: Maybe Bool
                       }

data QueryStale = Ok | UpdateAfter

instance ToJSON QueryStale where
    toJSON Ok = String "ok"
    toJSON UpdateAfter = String "update_after"


instance Default (Query a) where
    def = Query Nothing Nothing Nothing Nothing Nothing Nothing -- keep it
                Nothing Nothing Nothing Nothing Nothing Nothing -- coming
                Nothing Nothing Nothing Nothing

data QueryResult a k = QueryResult { qMeta :: QueryMeta 
                                   , qRows :: [ QueryRow a k ]
                                   }

data QueryDocsResult d a k = QueryDocsResult { qDMeta :: QueryMeta 
                                             , qDRows :: [ QueryDocsRow d a k ]
                                             }

data QueryMeta = QueryMeta { metaTotalRows     :: Int
                           , metaOffset        :: Int
                           , metaUpdateSeq     :: Maybe Integer
                           }
instance FromJSON QueryMeta where
    parseJSON (Object v) = QueryMeta <$> v .: "total_rows"
                                     <*> v .: "offset"
                                     <*> v .:? "update_seq"
    parseJSON _ = mzero

data QueryRow a k = QueryRow { rowValue :: a
                             , rowId    :: DocId
                             , rowKey   :: k
                             }

instance (FromJSON a, FromJSON k) => FromJSON (QueryRow a k) where
    parseJSON (Object v) = QueryRow <$> v .: "value"
                                    <*> v .: "id"
                                    <*> v .: "key"

data QueryDocsRow d a k = QueryDocsRow { dRowDoc   :: d
                                       , dRowValue :: a
                                       , dRowId    :: DocId
                                       , dRowKey   :: k
                                       }

instance (FromJSON d, FromJSON a, FromJSON k) => FromJSON (QueryDocsRow d a k) where
    parseJSON (Object v) = QueryDocsRow <$> v .: "doc"
                                        <*> v .: "value"
                                        <*> v .: "id"
                                        <*> v .: "key"

data Error = DBError { errorStatus   :: HTTP.Status
                     , errorText     :: T.Text
                     , errorReason   :: T.Text
                     }
           | InvalidDBResponse
           | UnknownException HttpException
           deriving Show

instance ToJSON Error where
    toJSON (DBError status text reason) = 
        object [ "status" .= HTTP.statusCode status
               , "error"  .= text
               , "reason" .= reason
               ]
    toJSON InvalidDBResponse = String "invalid_db_response"
    toJSON (UnknownException ex) = object [ "error" .= ("unknown_error" :: T.Text)
                                          , "exception" .= show ex
                                          ]

class Document a where
    getMetadata :: a -> Maybe Metadata
    applyMetadata :: a -> Metadata -> a

data Metadata = Metadata { metaId :: T.Text, metaRev :: T.Text } deriving Show

metadataToPairs :: Maybe Metadata -> [Pair]
metadataToPairs (Just (Metadata id rev)) = [ ("_id", String id)
                                           , ("_rev", String rev)
                                           ]
metadataToPairs Nothing = []

parseMetadata :: Object -> Parser (Maybe Metadata)
parseMetadata v = do
    id <- v .:? "_id"
    rev <- v.:? "_rev"
    case (id, rev) of 
        (Just id, Just rev) -> return $ Just $ Metadata id rev
        _ -> return $ Nothing


