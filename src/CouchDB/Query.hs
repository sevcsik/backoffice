module CouchDB.Query
    ( query
    , queryDocs
    ) where

import CouchDB.Types

import Network.HTTP.Client
import qualified Network.HTTP.Types as HTTP
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as LBS
import qualified Data.ByteString.Char8 as BS8
import qualified Data.HashMap.Strict as HMS
import qualified Data.Text as T
import Data.Text.Encoding
import Data.Aeson
import qualified Data.Aeson.Types as JSON
import Control.Exception
import Control.Monad

-- TODO: query and queryDocs could be more DRY

getQuery :: (ToJSON key) => Query key -> [(BS.ByteString, Maybe BS.ByteString)]
getQuery query = filter isempty
                        [ ( t "key"               , j queryKey)
                        , ( t "keys"              , j queryKeys)
                        , ( t "startkey"          , j queryStartkey)
                        , ( t "startkey_docid"    , j queryStartkeyDocid)
                        , ( t "endkey"            , j queryEndkey)
                        , ( t "endkey_docid"      , j queryEndkeyDocid)
                        , ( t "limit"             , j queryLimit)
                        , ( t "stale"             , j queryStale)
                        , ( t "descending"        , j queryDescending)
                        , ( t "skip"              , j querySkip)
                        , ( t "group"             , j queryGroup)
                        , ( t "group_level"       , j queryGroupLevel)
                        , ( t "reduce"            , j queryReduce)
                        , ( t "include_docs"      , j queryIncludeDocs)
                        , ( t "inclusuve_end"     , j queryInclusiveEnd)
                        , ( t "update_seq"        , j queryUpdateSeq)
                        ]
    where t = encodeUtf8
          j f = LBS.toStrict <$> encode <$> (f query)
          isempty ((_, Nothing)) = False
          isempty _ = True

query :: (ToJSON k, FromJSON k, FromJSON a)
         => Env
         -> Path
         -> Query k
         -> IO (Either Error (QueryResult a k))
query env path query = do
    res <- doQuery env path query { queryIncludeDocs = Just False }
    case res of
        Left error -> return $ Left error
        Right res' -> return $ parseResponse res'

queryDocs :: (ToJSON k, FromJSON k, FromJSON a, FromJSON d)
             => Env
             -> Path
             -> Query k
             -> IO (Either Error (QueryDocsResult d a k))
queryDocs env path query = do
    res <- doQuery env path query { queryIncludeDocs = Just True }
    case res of
        Left error -> return $ Left error
        Right res' -> return $ parseResponseDocs res'


doQuery :: (ToJSON k) => Env
                      -> Path
                      -> Query k
                      -> IO (Either Error (Response LBS.ByteString))
doQuery (Env settings mgr) path queryopts = do
    req <- parseUrl $ T.unpack $ T.concat $ [ settingsURL settings, path ]
    let req' = req { method = HTTP.methodGet
                   , queryString = HTTP.renderQuery True $ getQuery queryopts
                   , requestHeaders =
                       [(HTTP.hContentType
                       , encodeUtf8 "application/json; charset=utf-8"
                       )]
                   , checkStatus = \_ _ _ -> Nothing
                   }
    res <- try (httpLbs req' mgr)
    case res of Left ex -> return $ Left $ UnknownException $ ex
                Right res' -> return $ Right res'

parseResponse :: (FromJSON a, FromJSON k) => Response LBS.ByteString
                                          -> Either Error (QueryResult a k)
parseResponse res = case getError res of
    Just error -> Left error
    Nothing -> case (meta, rows) of
        (Just meta, Just (JSON.Success rows)) ->
            Right $ QueryResult meta rows
        _ -> Left InvalidDBResponse
    where
        body = responseBody res
        meta = decode body :: Maybe QueryMeta
        rows = fmap JSON.fromJSON rows'
            :: (FromJSON a, FromJSON k) => Maybe (JSON.Result [QueryRow a k])
        rows' = join $ fmap (HMS.lookup "rows") (decode body :: Maybe Object)
        -- TODO: we're decoding the same body twice. we should reuse 'value' to get rows'

parseResponseDocs :: (FromJSON d, FromJSON a, FromJSON k)
                     => Response LBS.ByteString
                     -> Either Error (QueryDocsResult d a k)
parseResponseDocs res = case getError res of
    Just error -> Left error
    Nothing -> case (meta, rows) of
        (Just meta, Just (JSON.Success rows)) ->
            Right $ QueryDocsResult meta rows
        _ -> Left InvalidDBResponse
    where
        body = responseBody res
        meta = decode body :: Maybe QueryMeta
        rows = fmap JSON.fromJSON rows'
            :: (FromJSON d, FromJSON a, FromJSON k)
            => Maybe (JSON.Result [QueryDocsRow d a k])
        rows' = join $ fmap (HMS.lookup "rows") (decode body :: Maybe Object)
        -- TODO: we're decoding the same body twice. we should reuse 'value' to get rows'

getError :: Response LBS.ByteString -> Maybe Error
getError res
    | statusCode >= 400 = case (parseError status) . responseBody $ res of
        Just error -> Just error
        Nothing -> Just InvalidDBResponse
    | otherwise = Nothing
    where status = responseStatus res
          statusCode = HTTP.statusCode status


parseError :: HTTP.Status -> LBS.ByteString -> Maybe Error
parseError status body = case (error, reason) of
    (Just (Just error), (Just (Just reason))) ->
        Just $ DBError status error reason
    _ -> Nothing
    where
        error = JSON.parseMaybe (\obj -> obj .: "error") <$> obj
        reason = JSON.parseMaybe (\obj -> obj .: "reason") <$> obj
        obj = decode body

