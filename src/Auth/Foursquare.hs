module Auth.Foursquare
    ( FoursquareLogin (..)
    , parseFsqLogin
    ) where

import Auth.OAuth
import Auth.Types
import EnvSettings
import qualified Model.User as U
import qualified CouchDB.Types as DB

import Data.Aeson
import Data.Aeson.Types
import Data.Maybe
import Control.Monad
import qualified Data.HashMap.Strict as HMS
import qualified Data.Text as T
import qualified Data.ByteString.Lazy as LBS
import Data.Text.Encoding
import Data.Time.Clock
import Data.Time.Clock.POSIX
import Data.Int

data FoursquareLogin = FoursquareLogin { fsqCode :: T.Text 
                                       , fsqNamespace :: T.Text
                                       , fsqOAuthSettings :: OAuthSettings
                                       }

instance AuthReq FoursquareLogin where
    authenticate = oAuthenticate
    namespace = fsqNamespace

instance OAuthLogin FoursquareLogin where
    view                = const "/_design/auth/_view/by_foursquare_id"
    identityEndpoint    = const "https://api.foursquare.com/v2/users/self"
    accessTokenEndpoint = const "https://api.foursquare.com/oauth2/authenticate"
    code                = fsqCode
    queryParams         = const [ ( encodeUtf8 "v"
                                  , Just $ encodeUtf8 "20150909")
                                ]
    parseIdentity       = const parseFsqUser
    createAuth _ fsqId  = U.FoursquareAuth fsqId
    settings            = fsqOAuthSettings

parseFsqLogin :: LBS.ByteString -> OAuthSettings -> Maybe FoursquareLogin
parseFsqLogin json settings = join $
    (parseMaybe $ \v -> do
        code <- v.: "code"
        namespace <- v.: "namespace"
        
        return $ FoursquareLogin code namespace settings
    ) <$> decode json

parseFsqUser :: LBS.ByteString -> Maybe (T.Text, U.Profile, [U.Contact])
parseFsqUser json = join $ 
    (parseMaybe $ \v -> do
        resp <- v .: "response"
        user <- resp .: "user"
        userId <- user .: "id"
        first <- user .: "firstName"
        last <- user .: "lastName"
        gender <- user .: "gender"
        contact <- user .: "contact"
        email <- contact .: "email"
        birthDate <- user .: "birthday" :: Parser Int64
        photo <- user .: "photo"
        photoPrefix <- photo .: "prefix"
        photoSuffix <- photo .: "suffix"

        let birthDate' = posixSecondsToUTCTime $ realToFrac $ birthDate
        let photo' = photoPrefix `mappend` "500x500" `mappend` photoSuffix
        let gender' = parseMaybe parseJSON gender

        return ( userId
               , U.Profile { U.profileFirstName  = Just first
                           , U.profileLastName   = Just last
                           , U.profilePictureURL = Just photo'
                           , U.profileGender     = gender'
                           , U.profileBirthDate  = Just birthDate'
                           }
               -- 4sq doesn't require email verification from users
               , [ U.EmailContact email False Nothing ]
               )
    ) <$> decode json
