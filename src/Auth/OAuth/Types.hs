module Auth.OAuth.Types where

import EnvSettings
import Model.User as U

import Data.ByteString.Lazy as LBS
import Data.ByteString as BS
import Data.Text as T
import Data.Default.Class



type Identity = (T.Text, U.Profile, [U.Contact])

class OAuthLogin a where
    view :: a -> T.Text -- | Path to the CouchDB views which maps external user IDs
                        --   to internal one
    accessTokenEndpoint :: a -> T.Text -- | Full URL of the API endpoint
                                       --   where auth code can be exchanged
                                       --   for an access token (oauth/authenticate)
    identityEndpoint :: a -> T.Text -- | Full URL of the API endpoint which provides
                                    --   the identity
    code :: a -> T.Text -- | Get OAuth authorization code
    -- | Additional query parameters for requesting the access token
    --   (excluding client_id and response_type)
    tokenParamName :: a -> T.Text -- | Name of the OAuth token query parameter
    -- | List of additional query parameters to be passed to identityEndpoint,
    --   excluding the token parameter
    queryParams :: a -> [(BS.ByteString, Maybe BS.ByteString)]     
    -- | Function which parses the response of `identityEndpoint`, and
    --   returns the external user id and an array of contacts
    parseIdentity :: a -> (LBS.ByteString -> Maybe Identity)
    -- | Create an auth record based on the external user id
    createAuth :: a -> T.Text -> U.Auth
    -- | Get an OAuthSettings instance from a login
    settings :: a -> OAuthSettings
    queryParams = const []
    tokenParamName = const "oauth_token"

data OAuthSettings = OAuthSettings { oAuthClientId :: T.Text
                                   , oAuthClientSecret :: T.Text
                                   }

instance EnvSettings OAuthSettings where
    set "CLIENT_ID" val a = a { oAuthClientId = T.pack val }
    set "CLIENT_SECRET" val a = a { oAuthClientSecret = T.pack val }
    parse prefix = updateWithList prefix [ "CLIENT_ID"
                                         , "CLIENT_SECRET"
                                         ]

instance Default OAuthSettings where
    def = OAuthSettings "" ""

