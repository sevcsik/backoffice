module Auth.Types
    ( AuthError(..)
    , AuthReq(..)) where

import qualified CouchDB as DB
import Model.User (Namespace)
import Settings
import Managers

import Data.Aeson
import Data.Text
import Network.HTTP.Client

data AuthError = QueryError DB.Error
               | UpdateError DB.Error
               | RequestFailed HttpException
               | InvalidResponse
               | NotFound
               | InvalidCredentials

instance ToJSON AuthError where
    toJSON (QueryError error) =
        object [ "type" .= ("query_error" :: Text)
               , "error" .= error
               ]
    toJSON (UpdateError error) =
        object [ "type" .= ("update_error" :: Text)
               , "error" .= error
               ]
    toJSON (RequestFailed ex) =
        object [ "type" .= ("request_failed" :: Text)
               , "error" .= show ex
               ]
    toJSON InvalidResponse =
        object [ "type" .= ("invalid_response" :: Text) ]
    toJSON NotFound =
        object [ "type" .= ("not_found" :: Text) ]
    toJSON InvalidCredentials =
        object [ "type" .= ("invalid_credentials" :: Text) ]
class AuthReq a where
    authenticate :: Settings -> Managers -> a -> IO (Either AuthError DB.DocId)
    namespace :: a -> Namespace
