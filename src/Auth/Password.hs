module Auth.Password
    ( PasswordLogin (..) ) where

import Model.User ( Auth (PasswordAuth) )
import CouchDB hiding ( Settings )
import Auth.Types
import Settings
import Managers

import Crypto.BCrypt ( validatePassword )
import Data.Aeson
import qualified Data.Text as T
import Data.Text.Encoding
import Data.Maybe

checkPassword :: T.Text -> QueryRow Auth (T.Text, T.Text) -> Maybe DocId
checkPassword password (QueryRow (PasswordAuth _ passhash _) id _) =
    if validatePassword passhash (encodeUtf8 password)
        then Just id
        else Nothing

authenticate' :: Settings -> Managers -> PasswordLogin -> IO (Either AuthError DocId)
authenticate' st mgrs (PasswordLogin namespace username password) = do
    let dbenv = Env (stUserDB st) (mgrDB mgrs)
    res <- query dbenv "/_design/auth/_view/by_password_auth"
           def { queryKey = Just (namespace, username) } 
                :: IO (Either Error (QueryResult Auth (T.Text, T.Text)))
    case res of
        Left error -> 
            return $ Left $ QueryError error
        Right (QueryResult _ auths) ->
            if (null auths) then return $ Left $ NotFound
            else case listToMaybe $ mapMaybe (checkPassword password) auths of
                Just userId -> return $ Right $ userId
                Nothing -> return $ Left $ InvalidCredentials

data PasswordLogin = PasswordLogin { pwLoginNamespace :: T.Text
                                   , pwLoginUsername :: T.Text
                                   , pwLoginPassword :: T.Text
                                   }

instance AuthReq PasswordLogin where
    authenticate = authenticate'
    namespace = pwLoginNamespace

instance FromJSON PasswordLogin where
    parseJSON (Object v) =
        PasswordLogin <$> v .: "namespace"
                      <*> v .: "username"
                      <*> v .: "password"
