module Auth.OAuth
    ( oAuthenticate
    , OAuthLogin (..)
    , OAuthSettings (..)
    , module Auth.OAuth.Types
    ) where

import Auth.OAuth.Types
import qualified Model.User as U
import Auth.Types
import CouchDB as DB
import EnvSettings
import Settings as ST
import Managers

import Data.List
import Data.Default.Class
import Network.HTTP.Client
import Network.HTTP.Types
import qualified Data.Text as T
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as LBS
import Data.Text.Encoding
import Data.Maybe
import Data.Aeson
import Data.Aeson.Types
import Control.Exception
import Control.Monad

-- | Fetches identity using the OAuth token and finds the matching user.
--   If no user is found, it creates a new user and sets it as an authentication
--   method. If a user is found with he same email, adds the authentication
--   method to the given user and returns it.
oAuthenticate :: (AuthReq a, OAuthLogin a) 
              => ST.Settings -- | Global settings
              -> Managers
              -> a   -- | Auth request
              -> IO (Either AuthError DocId)

oAuthenticate st mgrs login = do
    let env = Env (stUserDB st) (mgrDB mgrs)
    token <- getAuthToken (mgrAPI mgrs) login
    case token of 
        Left error -> return $ Left error
        Right token -> do
            identity <- getIdentity env login token
            case identity of 
                Left error -> return $ Left error
                Right identity -> do
                    user <- getUserByExtId env login identity
                    case user of 
                        Right (Just userid) -> return $ Right userid
                        Left error -> return $ Left error
                        Right Nothing -> do
                            user <- getUserByEmail env login identity
                            case user of
                                Right (Just userid) -> 
                                    return $ Right userid
                                Left error -> 
                                    return $ Left error
                                Right Nothing -> 
                                    createUser env login identity

getAuthToken :: (OAuthLogin a) => Manager 
                               -> a 
                               -> IO (Either AuthError T.Text)
getAuthToken mgr login = do
    req <- parseUrl $ T.unpack $ accessTokenEndpoint login
    let req' = req { queryString = renderQuery True $ params }
    -- TODO: we shouldn't use CouchDB's manager here.
    res <- try $ httpLbs req' mgr
    case res of
        Left ex -> return $ Left $ RequestFailed ex
        Right res -> case parseToken res of
            Nothing -> return $ Left $ InvalidResponse
            Just token -> return $ Right token

    where params = [ (e "client_id"     , Just $ e $ oAuthClientId st)
                   , (e "client_secret" , Just $ e $ oAuthClientSecret st)
                   , (e "redirect_uri"  , Just $ e "REDIRECT_URI")
                   , (e "grant_type"    , Just $ e "authorization_code")
                   , (e "code"          , Just $ e $ code login)
                   ]
          st = settings login
          e = encodeUtf8
          parseToken response = join $ parseMaybe 
                                       (\v -> v .: "access_token") 
                                       <$> (decode $ responseBody response)

getIdentity :: (OAuthLogin a) => Env -> a -> T.Text
                              -> IO (Either AuthError Identity)
getIdentity env login token = do
    req <- parseUrl $ T.unpack $ identityEndpoint login
    let req' = req { queryString = renderQuery True $ 
                       queryParams login ++ [ ( encodeUtf8 $ tokenParamName login
                                              , Just $ encodeUtf8 token
                                              )
                                            ]
                   }
    -- TODO: we shouldn't use CouchDB's manager here.
    res <- try $ httpLbs req' (envManager env)
    case res of
        Left ex -> return $ Left $ RequestFailed ex
        Right res ->
            case parseIdentity login $ responseBody res of
                Nothing -> return $ Left InvalidResponse
                Just identity -> return $ Right identity

getUserByExtId :: (OAuthLogin a, AuthReq a) => Env -> a -> Identity
                                            -> IO (Either AuthError (Maybe DocId))
getUserByExtId env login (extId, _, _) = do
    queryResult <- query env (view login)
                            def { queryKey = Just (namespace login, extId) }
                            :: IO (Either Error
                                          (QueryResult DocId ( U.Namespace
                                                             , T.Text)))
    case queryResult of
        Left error -> return $ Left $ QueryError error
        Right (QueryResult _ rows) ->
            case rows of [] -> return $ Right Nothing
                         _  -> return $ Right $ Just $ rowId $ head rows

getUserByEmail :: (OAuthLogin a, AuthReq a) => Env -> a -> Identity
                                            -> IO (Either AuthError (Maybe DocId))
getUserByEmail env login (extId, _, contacts) = do
    case find isVerifiedEmail contacts of
        Nothing -> return $ Right Nothing
        Just (U.EmailContact email True _) -> do
            queryResult <- query env "/_design/auth/_view/by_email"
                            def { queryKey = Just (namespace login, email) }
            case queryResult of
                Left error -> return $ Left $ QueryError error
                Right (QueryResult _ rows) ->
                    if null rows then return $ Right Nothing
                    else do
                        let userId = rowValue $ head rows
                        -- TODO: use CouchDB.get when implemented
                        users <- queryUserById userId
                        case users of
                            Left error ->
                                return $ Left $ QueryError error
                            -- TODO: this should not happen - find a better
                            -- way to report it
                            Right (QueryDocsResult _ []) ->
                                return $ Left $ NotFound
                            Right result -> do
                                updateError <- updateUserWithAuth env 
                                                                  login
                                                                  extId
                                                                  result
                                case updateError of
                                    Just error -> return $ Left $ UpdateError error
                                    Nothing -> return $ Right $ Just $ userId

    where queryUserById :: T.Text 
                        -> IO (Either Error (QueryDocsResult U.User Rev DocId)) 
          queryUserById userId = queryDocs env
                                           "/_all_docs" 
                                           def { queryKey = Just userId
                                               , queryIncludeDocs = Just True
                                               } 
 
updateUserWithAuth :: (AuthReq a, OAuthLogin a) 
                   => Env -> a -> T.Text
                   -> QueryDocsResult U.User Rev DocId
                   -> IO (Maybe Error)
updateUserWithAuth env login extId
                   (QueryDocsResult _ ((QueryDocsRow user _ _ _):_)) = do
    let user' = user { U.auths = U.auths user ++ [ createAuth login extId ] }
    res <- save env user'
    case res of
        Left error ->
            return $ Just error
        _ -> return $ Nothing


createUser :: (OAuthLogin a, AuthReq a) => Env -> a -> Identity
                                        -> IO (Either AuthError DocId)
createUser env login (extId, profile, contacts) = do
    let user = U.User { U.namespaces = [ namespace login ]
                      , U.auths = [ createAuth login extId ]
                      , U.contacts = contacts
                      , U.profile = profile
                      , U.metadata = Nothing
                      }
    user' <- save env user
    case user' of
        Left error -> return $ Left $ UpdateError error
        Right user -> return $ Right $ metaId $ fromJust $ getMetadata user

isVerifiedEmail (U.EmailContact _ True _) = True
isVerifiedEmail _ = False

