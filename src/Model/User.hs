module Model.User
    ( applyHashPasswordAuth
    , validate
    , createFromJSON
    , User (..)
    , Error (..)
    , Auth (..)
    , Contact(..)
    , Namespace
    , module Model.User.Profile
    ) where

import CouchDB.Types hiding ( Error )
import Model.User.Profile

import Control.Monad
import Control.Applicative
import Data.Maybe 
import Data.Text ( Text )
import qualified Data.Text as T
import Data.Text.Encoding 
    ( encodeUtf8 
    , decodeUtf8
    )
import Text.Regex.TDFA
import Data.ByteString ( ByteString )
import qualified Text.Email.Validate as EV
    ( emailAddress
    , isValid
    , EmailAddress
    )
import qualified Crypto.BCrypt as BC
    ( hashPasswordUsingPolicy 
    , fastBcryptHashingPolicy
    , validatePassword
    )
import Data.Aeson
    ( FromJSON
    , parseJSON
    , ToJSON
    , toJSON
    , (.=)
    , (.:)
    , Value ( Object, String )
    , decode
    )
import qualified Data.Aeson.Types as JSON
import qualified Data.HashMap.Strict as HMS
import qualified Data.ByteString.Lazy as LBS

-- TODO: break into properly prefixed submodules

data User = User
    { namespaces :: [Namespace]
    , contacts :: [Contact]
    , auths :: [Auth]
    , profile :: Profile
    , metadata :: Maybe Metadata
    } deriving Show

instance ToJSON User where
    toJSON (User namespaces contacts auths profile meta) =
        JSON.object $ [ "namespaces" .= namespaces
                      , "contacts"   .= contacts
                      , "auths"      .= auths
                      , "profile"    .= profile
                      ] ++ metadataToPairs meta

instance FromJSON User where
    parseJSON (Object v) =
        User <$> v .: "namespaces"
             <*> v .: "contacts"
             <*> v .: "auths"
             <*> v .: "profile"
             <*> (parseMetadata v)
    parseJSON _ = mzero 
     
instance Document User where
    getMetadata = metadata
    applyMetadata user metadata = user { metadata = Just metadata }

type Namespace = Text

data Contact = EmailContact
    { emailAddress :: Text
    , verified :: Bool
    , verificationToken :: Maybe ByteString
    } deriving Show

instance ToJSON Contact where
    toJSON (EmailContact emailAddress verified verificationToken) =
        JSON.object [ "type"              .= ("email" :: Text)
                    , "emailAddress"      .= emailAddress
                    , "verified"          .= verified
                    , "verificationToken" .= (fmap decodeUtf8 verificationToken)
                    ]

instance FromJSON Contact where
    parseJSON (Object v) =
        case contactType of 
            Just "email" -> EmailContact 
                <$> v .: "emailAddress"
                <*> v .: "verified"
                <*> (fmap . fmap) encodeUtf8 (v .: "verificationToken")
                    -- We need to convert Text to ByteString
                    -- over a FromJSON *and* a Maybe. Hence the double fmap
            _ -> mzero 
        where contactType = HMS.lookup "type" v

data Auth = PasswordAuth 
    { username :: Text
    , passHash :: ByteString
    , resetToken :: Maybe ByteString
    } 
    | PlainPasswordAuth
    { username :: Text
    , password :: Text
    }
    | FoursquareAuth
    { fsqId :: Text
    } deriving Show

instance ToJSON Auth where
    toJSON (PasswordAuth username passHash resetToken) =
        JSON.object [ "type"       .= ("password" :: Text)
                    , "username"   .= username
                    , "passHash"   .= (decodeUtf8 passHash)
                    , "resetToken" .= (fmap decodeUtf8 resetToken)
                    ]
    toJSON (FoursquareAuth id) = JSON.object [ "type" .= ("foursquare" :: Text)
                                             , "id"   .= id
                                             ]
    -- PlainPasswordAuth should never be serialized again

instance FromJSON Auth where
    parseJSON (Object v) =
        case authType of 
            Just "password" -> case password of
                Nothing -> PasswordAuth 
                    <$> v .: "username"
                    <*> fmap encodeUtf8 (v .: "passHash")
                    <*> (fmap . fmap) encodeUtf8 (v .: "resetToken")
                        -- We need to convert Text to ByteString
                        -- over a FromJSON *and* a Maybe. Hence the double fmap
                Just password -> PlainPasswordAuth
                    <$> v .: "username"
                    <*> v .: "password"
            Just "foursquare" -> FoursquareAuth <$> v.: "id"
            _ -> mzero 
        where authType = HMS.lookup "type" v
              password = HMS.lookup "password" v

data Error = InvalidJSON | InvalidUser [ValidationError] | PasswordHashFailure 
instance ToJSON Error where
    toJSON InvalidJSON = JSON.object [ "error" .= ("invalid_json" :: Text) ]
    toJSON (InvalidUser errors) = 
        JSON.object [ "error" .= ("invalid_user" :: Text)
                    , "reasons" .= errors
                    ]
    toJSON PasswordHashFailure = 
        JSON.object [ "error" .= ("password_hash_failure" :: Text) ]

data ValidationError = InvalidNamespace Namespace
                     | InvalidAuth AuthValidationError
                     | InvalidContact ContactValidationError
instance ToJSON ValidationError where
    toJSON (InvalidNamespace ns) = 
        JSON.object [ "error" .= ("invalid_namespace" :: Text) 
                    , "namespace" .= ns ]
    toJSON (InvalidAuth err) = 
        JSON.object [ "error" .= ("invalid_auth" :: Text)
                    , "auth" .= err ]
    toJSON (InvalidContact err) = 
        JSON.object [ "error" .= ("invalid_contact" :: String)
                    , "contact" .= err ]

data AuthValidationError = 
    InvalidPlainPasswordAuth Text [PlainPasswordAuthValidationError]
instance ToJSON AuthValidationError where
    toJSON (InvalidPlainPasswordAuth user reasons) =
        JSON.object [ "error" .= ("invalid_password_auth" :: Text)
                    , "username" .= user
                    , "reasons" .= reasons
                    ]

data PlainPasswordAuthValidationError = InvalidUsername
                                      | InvalidPassword
instance ToJSON PlainPasswordAuthValidationError where
    toJSON (InvalidUsername) = JSON.String "invalid_username"
    toJSON (InvalidPassword) = JSON.String "invalid_password"

data ContactValidationError = InvalidEmailContact Text
instance ToJSON ContactValidationError where
    toJSON (InvalidEmailContact addr) =
        JSON.object [ "error" .= ("invalid_email_contact" :: Text)
                    , "emailAddress" .= addr
                    ]

-- | Validate if a username matches the following criterias:
-- 
-- * 3-63 characters
-- * all visible characters and space is allowed
-- * cannot begin and end with whitespace
validateUsername :: Text -> Maybe PlainPasswordAuthValidationError 
validateUsername u = case result of True  -> Nothing
                                    False -> Just InvalidUsername
    where 
        result = (T.unpack u) =~ 
            ("[^[:space:]][^[:space:] ]{3,62}[^[:space:]]$" :: String)

-- | Validate if a password matches the following criterias
--
-- * 6-255 characters
validatePassword :: Text -> Maybe PlainPasswordAuthValidationError
validatePassword p = case result of True  -> Nothing
                                    False -> Just InvalidPassword
    where result = 6 <= (T.length p) && (T.length p) <= 255

validateAuth :: Auth -> Maybe AuthValidationError
validateAuth (PlainPasswordAuth user pass)
    | null errors = Nothing
    | otherwise = Just $ InvalidPlainPasswordAuth user errors
    where errors = maybeToList (validateUsername user)
                ++ maybeToList (validatePassword pass)
validateAuth (PasswordAuth _ _ _) = Nothing

validateContact :: Contact -> Maybe ContactValidationError
validateContact (EmailContact email _ _) 
    | EV.isValid $ encodeUtf8 email = Nothing
    | otherwise = Just $ InvalidEmailContact email

-- | Validate if a single namespace matches the following criterias:

-- * lowercase letters, numbers, and dash
-- * 3-255 characters,
-- * must start with a letter
validateNamespace :: Namespace -> Maybe Namespace
validateNamespace ns 
    | (T.unpack ns) =~ ("^[a-z][a-z0-9-]{2,255}$" :: String) = Nothing
    | otherwise = Just ns

validate :: User -> Maybe Error
validate (User namespaces contacts auths _ _) -- TODO: validate profile
    | null errors = Nothing 
    | otherwise = Just $ InvalidUser errors
    where
        errors = concat [nsErrors, authErrors, contactErrors]
        nsErrors = map InvalidNamespace $ 
            mapMaybe validateNamespace namespaces 
        authErrors = map InvalidAuth $
            mapMaybe validateAuth auths
        contactErrors = map InvalidContact $
            mapMaybe validateContact contacts

createFromJSON :: LBS.ByteString -> Either Error User
createFromJSON bs =
    case (decode bs :: Maybe User) of
        Nothing -> Left InvalidJSON
        Just user -> case (validate user) of
            Nothing -> Right user
            Just error -> Left error

-- | Generate hashed password auth from a plaintext password auth.
-- This uses OS entropy to salt the hash, hence the IO
-- return. Only the hash is stored, the password is
-- discarded. 
hashPasswordAuth :: Auth -> IO (Maybe Auth)
hashPasswordAuth (PlainPasswordAuth username password) = do
    hash <- BC.hashPasswordUsingPolicy BC.fastBcryptHashingPolicy $
        encodeUtf8 password -- generate password hash
    -- add a PasswordAuth authentication method to the user, with the hash
    return $ (PasswordAuth username) <$> hash <*> Just Nothing
hashPasswordAuth auth = return $ Just auth -- we leave others untouched
                                           -- so we can map this over all auths

-- | Finds any PlainPasswordAuths in a user, and replaces them with
-- PasswordAuths (see hashPasswordAuth, order is not preserved).
-- Returns IO (Nothing) if at least one of the 
-- hashPasswordAuth calls returns IO (Nothing)
applyHashPasswordAuth :: User -> IO (Either Error User)
applyHashPasswordAuth user@(User _ _ auths _ _) = do
    auths' <- sequence $ map hashPasswordAuth auths 
    case (filter isNothing auths') of
        [] -> return $ Right user { auths = (map fromJust auths') }
        _  -> return $ Left PasswordHashFailure
