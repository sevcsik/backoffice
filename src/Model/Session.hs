module Model.Session
    ( Session (..) ) where

import CouchDB.Types
import Model.User (Namespace)

import Data.Aeson
import Data.Time.Clock
import Control.Monad

data Session = Session { sessionNamespace :: Namespace
                       , sessionStart     :: UTCTime
                       , sessionEnd       :: UTCTime
                       , sessionUserId    :: DocId
                       , sessionMetadata  :: Maybe Metadata
                       } deriving Show

instance Document Session where
    getMetadata = sessionMetadata
    applyMetadata s m = s { sessionMetadata = Just m }

instance ToJSON Session where
    toJSON (Session ns start end userId meta) =
        object $ [ "namespace" .= ns
                 , "start"     .= start
                 , "end"       .= end
                 , "user_id"   .= userId
                 ] ++ metadataToPairs meta

instance FromJSON Session where
    parseJSON (Object v) = Session <$> v .: "namespace"
                                   <*> v .: "start"
                                   <*> v .: "end"
                                   <*> v .: "user_id"
                                   <*> (parseMetadata v)
    parseJSON _ = mzero
