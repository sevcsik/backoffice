module Model.Namespace
    ( Namespace (..)
    ) where

import CouchDB.Types

import Control.Monad
import Data.Aeson
import Data.Text

data Namespace = Namespace { nsOrigins :: [ Text ]
                           , nsMetadata :: Maybe Metadata
                           }

instance FromJSON Namespace where
    parseJSON (Object v) =
        Namespace <$> v.: "origins"
                  <*> (parseMetadata v)
    parseJSON _ = mzero

instance ToJSON Namespace where
    toJSON (Namespace origins meta) =
        object $ [ "origins" .= origins ] ++ metadataToPairs meta

instance Document Namespace where
    getMetadata = nsMetadata
    applyMetadata ns meta = ns { nsMetadata = Just meta }
