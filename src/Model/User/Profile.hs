module Model.User.Profile
    ( Profile (..)
    , Gender (..)
    ) where

import Data.Aeson.Types as JSON
import Control.Monad
import Data.Text
import Data.Time.Clock

data Profile = Profile { profileFirstName   :: Maybe Text
                       , profileLastName    :: Maybe Text
                       , profilePictureURL  :: Maybe Text
                       , profileGender      :: Maybe Gender
                       , profileBirthDate   :: Maybe UTCTime
                       } deriving Show

instance ToJSON Profile where
    toJSON (Profile first last pic gender birthDate) =
        object [ "picture"    .= pic
               , "firstName"  .= first
               , "lastName"   .= last
               , "gender"     .= gender
               , "birthDate"  .= birthDate
               ]

instance FromJSON Profile where
    parseJSON (Object v) = Profile <$> v .: "firstName"
                                   <*> v .: "lastName"
                                   <*> v .: "picture"
                                   <*> v .: "gender"
                                   <*> v .: "birthDate"
    parseJSON _ = mzero

data Gender = Male | Female | Other deriving Show

instance ToJSON Gender where
    toJSON Male   = JSON.String "male"
    toJSON Female = JSON.String "female"
    toJSON Other  = JSON.String "other"
    
instance FromJSON Gender where
    parseJSON (JSON.String "male") = return Male
    parseJSON (JSON.String "female") = return Female
    parseJSON (JSON.String "other") = return Other
    parseJSON _ = mzero
