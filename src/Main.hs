import Settings
import Managers
import Ctrl.Home
import Ctrl.User
import Ctrl.Login
import Ctrl.Namespace
import qualified CouchDB as DB

import Web.Scotty
import Network.Wai.Middleware.Static
import Network.HTTP.Client
import Network.HTTP.Client.TLS

defaultSettings = def
    { stUserDB = def { DB.settingsURL = "http://localhost:5984/sd-auth-users" }
    , stSessionDB = def { DB.settingsURL = "http://localhost:5984/sd-auth-sessions" }
    , stNamespacesDb = def { DB.settingsURL = "http://localhost:5984/sd-auth-namespaces" }
    }

main = do
    settings <- parse "SD_AUTH" defaultSettings
    managers <- createManagers
    -- TODO: create and pass managers
    scotty (stPort settings) $ do
        middleware $ staticPolicy $ addBase "static"
        middleware $ applyCORS (stNamespacesDb settings) (mgrDB managers)
        get "/" getHome
        post "/user" (postUser settings managers)
        post "/login" (postLogin settings managers)

