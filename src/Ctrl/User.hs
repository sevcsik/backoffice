module Ctrl.User 
    ( postUser ) where

import Settings
import Managers
import qualified Model.User as U
import qualified CouchDB as DB

import Web.Scotty
import Data.Text.Lazy ( pack )
import Network.HTTP.Types.Status
import Data.Aeson ( decode )
import Control.Monad.IO.Class ( liftIO )

postUser :: Settings -> Managers -> ActionM ()
postUser st mgrs = do
    let dbenv = DB.Env (stUserDB st) (mgrDB mgrs) 
    rawUser <- body
    case U.createFromJSON rawUser of
        Left error -> do 
            status badRequest400
            json error
        Right user -> do
            hashedUser <- liftIO $ U.applyHashPasswordAuth user
            case hashedUser of
                Left error -> do
                    status internalServerError500
                    json error
                Right user -> do
                    savedUser <- liftIO $ DB.save dbenv user
                    case savedUser of
                        Left error@(DB.DBError status' _ _) -> do
                            status status'
                            json error
                        Right user -> do
                            status created201
                            json user 
