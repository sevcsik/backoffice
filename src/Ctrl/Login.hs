module Ctrl.Login 
    ( postLogin ) where

import qualified CouchDB as DB
import Auth.Types
import Auth.Password
import Auth.Foursquare
import Session
import Model.Session
import Model.User (Namespace)
import Managers
import Settings

import Web.Scotty
import Web.Scotty.Cookie
import Web.Cookie
import Network.HTTP.Types
import Data.Aeson (encode, decode, FromJSON)
import Data.Aeson.Types as JSON
import Control.Monad
import Control.Monad.IO.Class (liftIO)
import Data.Maybe
import Data.Time.Clock
import Data.Default.Class
import Data.Text.Encoding
import qualified Data.HashMap.Strict as HMS
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as LBS

data LoginType = Password | Foursquare
instance FromJSON LoginType where
    parseJSON (Object v) = case logintype of 
        Just "password" -> return Password
        Just "foursquare" -> return Foursquare
        where logintype = HMS.lookup "type" v
    parseJSON _ = mzero

postLogin :: Settings -> Managers-> ActionM ()
postLogin st mgrs = do
    reqBody <- body
    let logintype = decode reqBody :: Maybe LoginType

    case logintype of
        Just Password -> doLogin (decode reqBody :: Maybe PasswordLogin)
        Just Foursquare -> doLogin $ parseFsqLogin reqBody (stFsq st)
        _ -> status badRequest400

    where 
        doLogin (Just login) = do
            result <- liftIO $ authenticate st mgrs login
            case result of
                Right userId -> do
                    session' <- liftIO $ 
                        createSession sdbenv 
                                      (namespace login) 
                                      userId
                    case session' of
                        Right session -> do
                            status ok200
                            setSessionCookie session
                            json session
                        Left error -> do
                            status internalServerError500
                            json error
                Left error -> do
                    case error of
                        InvalidCredentials -> status forbidden403
                        NotFound -> status notFound404
                        _ -> status internalServerError500
                    json error
        doLogin _ = status badRequest400
        udbenv = DB.Env (stUserDB st) (mgrDB mgrs)
        sdbenv = DB.Env (stSessionDB st) (mgrDB mgrs)         

setSessionCookie :: Session -> ActionM ()
setSessionCookie (Session ns _ end userid mmeta) =
    setCookie def { setCookieName = encodeUtf8 $ ns `mappend` "-session"
                  , setCookieValue = encodeUtf8 sessionId
                  , setCookieExpires = Just end
                  }
    where sessionId = DB.metaId $ fromJust $ mmeta
