module Ctrl.Namespace
    ( applyCORS
    ) where

import CouchDB
import Model.Namespace

import Network.Wai
import Network.Wai.Middleware.Cors
import Network.HTTP.Client hiding ( requestBody
                                  , requestHeaders
                                  , responseHeaders
                                  )
import Data.Aeson
import Data.Aeson.Types
import Control.Monad
import Data.Default.Class
import Data.Text.Encoding
import Data.Text (Text)
import Data.List
import Data.ByteString.Lazy (fromStrict)

applyCORS :: Settings -> Manager -> Middleware

applyCORS st mgr app req res = do
    body <- requestBody req
    case decodeNamespace $ fromStrict body of
        Nothing -> noop
        Just namespace -> do
            nsDoc <- queryNamespace st mgr namespace
            case nsDoc of
                Nothing -> noop
                Just nsDoc' ->
                    case (nsOrigins nsDoc', origin req) of
                        (origins', Just origin') ->
                            if (any (== origin') origins') 
                                then addCORS origin'
                                else noop
                        _ -> noop
    where noop = app req res

decodeNamespace lbs = join $
    parseMaybe (.: "namespace") <$> decode lbs

queryNamespace st mgr ns = do
    result' <- result
    case result' of
        Left _ -> return Nothing
        Right results -> case results of
            QueryDocsResult _ [] -> return Nothing
            QueryDocsResult _ rows -> return $ dRowValue $ head rows
    where result = queryDocs (Env st mgr) "/__all_docs"
                             def { queryKey = ns }
                   :: Either Error (QueryDocsResult Namespace () DocId)

origin req = fmap (decodeUtf8 . snd) $ (flip find) (requestHeaders req) $ 
    \header -> fst header == encodeUtf8 "Origin"

addCORS origin = cors simpleCorsResourcePolicy { corsOrigins }
    where corsOrigins = Just ([ encodeUtf8 origin ], True)
