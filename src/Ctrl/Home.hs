module Ctrl.Home ( getHome ) where

import Web.Scotty

getHome :: ActionM ()
getHome = do
    setHeader "Content-type" "text/html; charset=utf-8"
    file "static/index.html"
