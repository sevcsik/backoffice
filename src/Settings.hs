module Settings
    ( Settings(..)
    , module EnvSettings 
    , module Data.Default.Class
    ) where

import EnvSettings
import Auth.OAuth.Types (OAuthSettings (..))
import qualified CouchDB.Types as DB

import Data.Default.Class
import Network.Wai.Handler.Warp (Port)
import System.Environment

data Settings = Settings { stPort         :: Port
                         , stHost         :: String 
                         , stTLSKey       :: Maybe FilePath
                         , stTLSCert      :: Maybe FilePath
                         , stUserDB       :: DB.Settings
                         , stSessionDB    :: DB.Settings
                         , stNamespaceDB  :: DB.Settings
                         , stFsq          :: OAuthSettings
                         }

instance Default Settings where
    def = Settings { stPort        = 3000
                   , stHost        = "127.0.0.1"
                   , stTLSKey      = Nothing
                   , stTLSCert     = Nothing
                   , stUserDB      = def
                   , stSessionDB   = def
                   , stNamespaceDB = def
                   , stFsq         = def
                   }

instance EnvSettings Settings where
    set "PORT" val a = a { stPort = read val } 
    set "HOST" val a = a { stHost = val } 
    set "TLS_KEY_PATH" val a = a { stTLSKey = Just val } 
    set "TLS_CERT_PATH" val a = a { stTLSCert = Just val } 
    parse prefix a = do
        a' <- updateWithList prefix
                             [ "PORT"
                             , "HOST"
                             , "TLS_KEY_PATH"
                             , "TLS_CERT_PATH"
                             ]
                             a

        stUserDB' <- parse (compose a prefix "SESSION_DB") $ stUserDB a
        stSessionDB' <- parse (compose a prefix "USER_DB") $ stSessionDB a
        stNamespaceDB' <- parse (compose a prefix "NAMESPACE_DB") $ stNamespaceDB a
        stFsq' <- parse (compose a prefix "FSQ_OAUTH") $ stFsq a
        return a' { stUserDB = stUserDB'
                  , stSessionDB = stSessionDB'
                  , stFsq = stFsq'
                  }
