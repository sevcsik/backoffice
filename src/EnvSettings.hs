module EnvSettings (EnvSettings ( parse
                                , set
                                , update
                                , updateWithList
                                , compose
                                )) where

import System.Environment
import Data.List

class EnvSettings a where
    -- | Parse settings from environment vars and update initial
    parse :: String                     -- | Environment variable prefix
          -> a                          -- | Initial value
          -> IO a

    -- | Update an EnvSettings instance with a value
    set :: String                       -- | Variable name (unprefixed)
        -> String                       -- | Variable value
        -> a                            -- | The instance
        -> a                            

    -- | Update settings with a single environment variable
    update :: String                    -- | Prefix
           -> String                    -- | Variable name
           -> a                         -- | The instance
           -> IO a                      -- | The instance with an updated field
    update p var a = do
        menv <- lookupEnv $ compose a p var
        case menv of 
            Just value -> do
                -- do not log sensitive stuff
                if sensitive
                    then putStrLn $ (compose a p var) ++ "=<censored>"
                    else putStrLn $ (compose a p var) ++ "=" ++ value
                return $ set var value a
            Nothing -> do
                putStrLn $ (compose a p var) ++ " is unset, using default"
                return a
            where sensitive = foldr (||) False isSensitive 
                  isSensitive = map (flip isSubsequenceOf $ var)
                                ["KEY", "TOKEN", "SECRET"]

    -- | Update EnvSettings instance from a list of environment variables
    updateWithList :: String            -- | Variable name prefix
                   -> [String]          -- | Unprefixed env variable names
                   -> a                 -- | Initial EnvSettings instance
                   -> IO a              -- | Updated EnvSettings instance

    updateWithList prefix vars a =
        foldr (\var acc -> acc >>= update prefix var) (return a) vars

    -- | Compose an environment variable name
    compose :: a                        -- | The instance
            -> String                   -- | The prefix
            -> String                   -- | The variable name
            -> String                   -- | Prefixed variable name
    compose _ p s = p ++ "_" ++ s

