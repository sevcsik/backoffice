module CouchDB
    ( Document (..)
    , Metadata (..)
    , Error (..)
    , Env (..)
    , save
    , module CouchDB.Query
    , module CouchDB.Types
    ) where

import CouchDB.Query
import CouchDB.Types

import Data.Maybe
import qualified Data.Text as T 
import qualified Data.Text.Encoding as TE
import qualified Data.ByteString.Lazy as LBS
import qualified Data.ByteString as BS
import Control.Applicative
import Control.Exception
import Network.HTTP.Types.Header
import Network.HTTP.Types.Method
import Network.HTTP.Types.Status
import Network.HTTP.Client
    ( Manager
    , Request (..)
    , RequestBody (..)
    , Response (..)
    , newManager
    , defaultManagerSettings
    , parseUrl
    , httpLbs
    , responseBody
    , responseStatus
    , HttpException ( StatusCodeException )
    )
import Data.Aeson 
    ( ToJSON (..)
    , FromJSON (..)
    , Object
    , object
    , decode
    , encode
    , (.:?)
    , (.:)
    , (.=)
    )
import Data.Aeson.Types
    ( Pair
    , Parser (..)
    , Value (..)
    , parseMaybe
    )

save :: (ToJSON a, FromJSON a, Document a) => Env -> a -> IO (Either Error a)
save env@(Env settings mgr) doc
    | isNothing $ getMetadata doc =
        doSaveRequest env methodPost (settingsURL settings) doc
    | otherwise = 
        doSaveRequest 
            env
            methodPut 
            (T.concat [ settingsURL settings, "/"
                      , metaId $ fromJust $ getMetadata doc]
            )
            doc

defaultHeaders = [ (hContentType, "application/json; charset=utf-8") ]

doSaveRequest env method url doc = do
    let mgr = envManager env
    req <- parseUrl $ T.unpack url
    let req' = req { method = method
                   , requestHeaders = defaultHeaders 
                   , requestBody = RequestBodyLBS $ encode doc
                   -- we want to treat error responses as normal ones
                   , checkStatus = (\_ _ _ -> Nothing) 
                   }
    logRequest env req'
    res <- try (httpLbs req' mgr)
    case res of
        Left ex -> handleError ex
        Right res -> do
            logResponse env res
            return $ parseSaveResponse res doc
    where
        handleError ex = return $ Left $ UnknownException ex
        parseSaveResponse res doc
            | statusCode status >= 400 = case (error, reason) of
                (Just (Just error), Just (Just reason)) ->
                    Left $ DBError status error reason
                _ ->
                    Left $ DBError status "invalid_json" "failed to parse JSON response"
            | otherwise = case (id, rev) of
                (Just (Just id), Just (Just rev)) -> 
                    Right $ applyMetadata doc $ Metadata id rev
                _ -> 
                    Left $ DBError status "invalid_json" "failed to parse JSON response"
            where
                status = responseStatus res
                error = parseMaybe (\obj -> obj .: "error") <$> obj
                reason = parseMaybe (\obj -> obj .: "reason") <$> obj
                id = parseMaybe (\obj -> obj .: "id") <$> obj
                rev = parseMaybe (\obj -> obj .: "rev") <$> obj
                obj = decode $ responseBody res

logRequest env req = if enabled 
    then putStrLn $
           "-- CouchDB REQ --\n" 
        ++ show req
        ++ "-- EOR --"
    else return ()
    where enabled = settingsLogRequests $ envSettings $ env

logResponse env res = if enabled 
    then putStrLn $
           "-- CouchDB RES --\n" 
        ++ show res
        ++ "-- EOR --"
    else return ()
    where enabled = settingsLogResponses $ envSettings $ env
